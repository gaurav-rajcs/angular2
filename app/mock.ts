import{ CarPart } from './car-parts';

export const CARPARTS: CarPart[] =[{
    "id": 1,
    "name": "Super Tires",
    "description":"These tires are best",
    "instock": 5,
    "price":4.99,
    "image":"/images/tire.jpg"
  },
  {
    "id": 2,
    "name": "Reinforced Shocks",
    "description":"Shocks made from kryptonite",
    "instock": 4,
    "price":9.99,
    "image":"/images/shocks.jpg"
  },
  {
    "id": 3,
    "name": "Padded Seats",
    "description":"Super soft seat for smooth ride",
    "instock": 0,
    "price":15.99,
    "image":"/images/seats.jpg"
  }];